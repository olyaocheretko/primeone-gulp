function testWebP(callback) {
  var webP = new Image();
  webP.onload = webP.onerror = function () {
    callback(webP.height == 2);
  };
  webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
}
testWebP(function (support) {
  if (support == true) {
    document.querySelector('html').classList.add('_webp');
  } else {
    document.querySelector('html').classList.add('_no-webp');
  }
});

@@include('_responsive.js')

//Add class on click for menu_icon
$('.header-menu__icon').click(function(event){
	$(this).toggleClass('active');
	$('.header-menu').toggleClass('active');
	$('body').toggleClass('lock');
});


//Add google map
let map;

function initMap() {
  const myLatLng = { lat: 40.74434276419618, lng: -73.9261395241616 };
  map = new google.maps.Map(document.getElementById("map"), {
    center: myLatLng,
    zoom: 17,
  });
  //add marker
  let marker = new google.maps.Marker({
    position: myLatLng,
    //on which map to place the marker
    map: map,
    title: "PrimeOne",
	});
}